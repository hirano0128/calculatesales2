package jp.alhinc.hirano_ryuya.calculate_sales;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class CalculateSales {


	public static void main(String[] args) {

		//支店定義マップ
		Map<String, String> branch = new TreeMap<String, String>();
		//支店売り上げマップ
		Map<String, Long> branchsale = new TreeMap<String, Long>();
		//商品定義マップ
		Map<String, String> commodity = new TreeMap<String, String>();
		//商品売り上げマップ
		Map<String, Long> commoditysale = new TreeMap<String, Long>();
		//支店コードリスト
		List<String> branchnumber = new ArrayList<String>();
		//商品コードリスト
		List<String> commoditynumber = new ArrayList<String>();

		//コマンドライン引数が正しく渡されていることの確認
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		if(!in(args, branch, branchsale, branchnumber,"branch.lst","支店")) {
			return;
		}

		if(!in(args, commodity, commoditysale, commoditynumber,"commodity.lst","商品")) {
			return;
		}


		BufferedReader br = null;

		//売り上げファイルの設定
		List<File> saleFile = new ArrayList<File>();

		// フォルダを開く
		File dir = new File(args[0]);

		// フォルダに存在するファイルまたはフォルダの一覧を取得する
		File[] fileList = dir.listFiles();

		// 連番チェック用リスト(int)
		List<Integer> number = new ArrayList<Integer>();

		//一覧から名前を取得
		for (File file : fileList) {
			// ファイルの拡張子が「.rcd」且つファイル名が数字8桁になっていることを確認する
			if ( file.getName().matches("^[0-9]{8}\\.rcd$") && file.isFile() ) {
				saleFile.add(file);
				number.add( Integer.parseInt( file.getName().substring(0,8) ) );
			}
		}
		// 昇順にソート
		Collections.sort(number);

		// 売上ファイル名の連番チェック
		for (int i = 1; i < number.size(); i++) {
			if ( number.get(i) != number.get(i-1) + 1 ) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		// 売上ファイルの情報を1行ずつ保存する

		long branchsales =0;
		long commoditysales =0;
		for (int i = 0; i < saleFile.size(); i++) {

			List<String> line = new ArrayList<String>();

			try {
				// ファイルを開く
				FileReader fr;
				fr = new FileReader( saleFile.get(i) );
				br = new BufferedReader(fr);
				String tmp;
				// 売上ファイルを1行ずつ読み込む
				while ( (tmp = br.readLine()) != null ) {
					line.add(tmp);
				}

				// 売上ファイルが3行であるか確認する
				if ( line.size() != 3 ) {
					System.out.println( saleFile.get(i).getName()+"のフォーマットが不正です");
					return;
				}

				// 売上金額が数値であることを確認する
				if ( !(line.get(2).matches("^[0-9]*$"))) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				//売上ファイルの支店コードが支店定義ファイルに存在を確認する。
				if(!(branch.containsKey(line.get(0)))) {
					System.out.println(saleFile.get(i).getName()+"の支店コードが不正です");
					return;
				}
				if(!(commodity.containsKey(line.get(1)))) {
					System.out.println(saleFile.get(i).getName()+"の商品コードが不正です");
					return;
				}
				//売上額の集計
				branchsales =branchsale.get(line.get(0)) + Long.parseLong(line.get(2));
				// 支店の合計金額の集計
				branchsale.put(line.get(0), branchsales);
				// 合計金額が10桁を超えていないことを確認する
				if (branchsales > 9999999999L ) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				//売上額の集計
				commoditysales = commoditysale.get(line.get(1)) + Long.parseLong(line.get(2));
				// 合計金額が10桁を超えていないことを確認する
				if (commoditysales > 9999999999L ) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				// 商品の合計金額の集計
				commoditysale.put(line.get(1), commoditysales);
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
					}
				}
			}
		}
		if(!out(branch, branchsale, dir, "branch.out")) {
			return;
		}

		if(!out(commodity, commoditysale, dir,"commodity.out")) {
			return;
		}

	}
	//集計ファイル出力メソッド
	public static  boolean out(Map<String, String> name , Map<String, Long> sale, File dir , String outfile) {

		FileWriter fw = null;
		BufferedWriter bw = null;
		PrintWriter pw = null;

		// フォルダを開く
		try {

			File file = new File(dir, outfile);

			fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			pw = new PrintWriter(bw);

				//集計の出力
			for (Entry<String, String> entry : name.entrySet()) {

				String key = entry.getKey();
				Object value = entry.getValue();

			pw.println(key + "," + value +  "," + sale.get(key));

			}

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;

		}finally {
			if(pw != null) {
				try {

					bw.close();
					pw.close();

				} catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;

	}

	public static  boolean in(String[] args,Map<String, String> inname , Map<String, Long> insale, List<String> innumber, String infile, String filename) {

		BufferedReader br = null;


		try {
			//支店定義ファイルを開く
			File file = new File(args[0],infile);

			if ( !(file.exists()) ) {
				System.out.println(filename + "定義ファイルが存在しません");
				return false;
			}

			// ファイルを開く
			FileReader fr =new FileReader(file);
			br = new BufferedReader(fr);

			String line ;

			while((line = br.readLine()) !=null) {
				// 文字列の分割
				String[] tmp = line.split(",", -1);

				// 分割した文字列が2つであることを確認する
				if ( tmp.length != 2 ) {
					System.out.println(filename + "定義ファイルaのフォーマットが不正です");
					return false;
				}

				// 支店コードが3桁の数字であることを確認する
				if (filename == "支店" && !(tmp[0].matches("^[0-9]{3}$")) ) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}

				// 商品コードが8桁の文字列であることを確認する
				if (filename == "商品" && !(tmp[0].matches("^[A-Z a-z 0-9]{8}$")) ) {
					System.out.println("商品定義ファイルのフォーマットが不正です");
					return false;
				}
				// 支店情報の登録
				inname.put(tmp[0],tmp[1]);
				insale.put(tmp[0], (long)0);
				innumber.add(tmp[0]);
			}

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(br != null) {
				try {
				br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
				}

			}
		}
		return true;


	}
}
