package jp.alhinc.hirano_ryuya.calculate_sales;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class CalculateSales {

	//支店定義マップ
	static Map<String, String> branch = new TreeMap<String, String>();
	//支店売り上げマップ
	static Map<String, String> branchsale = new TreeMap<String, String>();
	//支店売り上げマップ
	static Map<String, String> branchtest = new TreeMap<String, String>(Collections.reverseOrder());
	//商品定義マップ
	static Map<String, String> commodity = new TreeMap<String, String>();
	//商品売り上げマップ
	static Map<String, String> commoditysale = new TreeMap<String, String>();
	//支店コードリスト
	static List<String> branchnumber = new ArrayList<String>();
	//商品コードリスト
	static List<String> commoditynumber = new ArrayList<String>();

	public static void main(String[] args) {

		BufferedReader br =null;
		try {
			//支店定義ファイルを開く
			File file = new File(args[0],"branch.lst");

			// ファイルを開く
			FileReader fr =new FileReader(file);
			br = new BufferedReader(fr);

			String line ;

			while((line = br.readLine()) !=null) {
				// 文字列の分割
				String[] tmp = line.split(",", -1);

				// 支店コードが3桁の数字であることを確認する
				if ( tmp.length != 2 ) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				// 支店コードが3桁の数字であることを確認する
				if (!(tmp[0].matches("^[0-9]{3}$")) ) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				// 支店情報の登録
				branch.put(tmp[0],tmp[1]);
				branchsale.put(tmp[0], "0");
				branchnumber.add(tmp[0]);
			}

		} catch (FileNotFoundException fnfe) {
		    System.out.println("ファイルが存在しません");
		} catch(IOException e) {
			System.out.println("エラーが発生しました");
		} finally {
			if(br != null) {
				try {
				br.close();
				} catch(IOException e) {
				System.out.println("クローズできません");
				}

			}
		}

		try {
			//商品定義ファイルを開く
			File file = new File(args[0],"commodity.lst");

			// ファイルを開く
			FileReader fr =new FileReader(file);
			br = new BufferedReader(fr);

			String line ;

			while((line = br.readLine()) !=null) {
				// 文字列の分割
				String[] tmp = line.split(",", -1);

				// 分割した文字列が2つであることを確認する
				if ( tmp.length != 2 ) {
					System.out.println("商品定義ファイルのフォーマットが不正です");
					return;
				}

				// 商品コードが8桁の文字列であることを確認する
				if (!(tmp[0].matches("^[A-Z a-z 0-9]{8}$")) ) {
					System.out.println("商品定義ファイルのフォーマットが不正です");
					return;
				}
				// 商品情報の登録
				commodity.put(tmp[0],tmp[1]);
				commoditysale.put(tmp[0], "0");
				commoditynumber.add(tmp[0]);
			}

		} catch (FileNotFoundException fnfe) {
		    System.out.println("ファイルが存在しません");
		} catch(IOException e) {
			System.out.println("エラーが発生しました");
		} finally {
			if(br != null) {
				try {
				br.close();
				} catch(IOException e) {
				System.out.println("クローズできません");
				}

			}
		}
		//売り上げファイルの設定
		List<File> saleFile = new ArrayList<File>();

		// フォルダを開く
		File dir = new File(args[0]);

		// フォルダに存在するファイルまたはフォルダの一覧を取得する
		File[] fileList = dir.listFiles();

		// 連番チェック用リスト(int)
		List<Integer> number = new ArrayList<Integer>();

		//一覧から名前を取得
		for (File file : fileList) {
			// ファイルの拡張子が「.rcd」且つファイル名が数字8桁になっていることを確認する
			if ( file.getName().matches("^[0-9]{8}\\.rcd$") ) {
				saleFile.add(file);
				number.add( Integer.parseInt( file.getName().substring(0,8) ) );
			}
		}
		// 昇順にソート
		Collections.sort(number);
		for (int i = 1; i < number.size(); i++) {
			if ( number.get(i) != number.get(i-1) + 1 ) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		// 売上ファイルの情報を1行ずつ保存する
		List<String> line = new ArrayList<String>();
		long branchsales =0;
		long commoditysales =0;
		for (int i = 0; i < saleFile.size(); i++) {
			try {
				// ファイルを開く
				FileReader fr;

				fr = new FileReader( saleFile.get(i) );


				 br = new BufferedReader(fr);


				String tmp;
				// 売上ファイルを1行ずつ読み込む
				while ( (tmp = br.readLine()) != null ) {
					line.add(tmp);
				}


				// 売上ファイルが4行以上ある場合を確認する
				if ( line.size() > 3 ) {
					System.out.println( saleFile.get(i).getName()+"のフォーマットが不正です");
					return;
				}


				int branchCount = 0;	// 支店コードの不一致回数
				int commodityCount = 0;	// 商品コードの不一致回数

				//支店コードの不一致回数集計
				for (int j = 0; j < branch.size(); j++) {
					if (!(branchnumber.get(j).equals(line.get(0)))) {
						branchCount++;
					}
				}

				// 支店コードが一致しなかった場合
				if ( branchCount == branch.size() ) {
					System.out.println(saleFile.get(i).getName()+"の支店コードが不正です");
					return;
				}
				for (int j = 0; j < branch.size(); j++) {
					if (!(branchnumber.get(j).equals(line.get(0)))) {
						branchCount++;
					}
				}

				// 支店コードが一致しなかった場合
				if ( branchCount == branch.size() ) {
					System.out.println(saleFile.get(i).getName()+"の支店コードが不正です");
					return;
				}

				// 商品コードが一致しなかった場合
				if ( commodityCount == commodity.size() ) {
					System.out.println(saleFile.get(i).getName()+"の商品コードが不正です");
					return;
				}
				for (int j = 0; j < commodity.size(); j++) {
					if (!(commoditynumber.get(j).equals(line.get(1)))) {
						commodityCount++;
					}
				}

				// 商品コードが一致しなかった場合
				if ( commodityCount == commodity.size() ) {
					System.out.println(saleFile.get(i).getName()+"の商品コードが不正です");
					return;
				}

				//売上額の集計
				branchsales = Long.parseLong(branchsale.get(line.get(0))) + Long.parseLong(line.get(2));

				// 支店の合計金額の集計
				branchsale.put(line.get(0), String.valueOf(branchsales));

				// 合計金額が10桁を超えていないことを確認する
				if (branchsales > 9999999999L ) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				//売上額の集計
				commoditysales = Long.parseLong(commoditysale.get(line.get(1))) + Long.parseLong(line.get(2));

				// 商品の合計金額の集計
				commoditysale.put(line.get(1), String.valueOf(commoditysales));

				// 合計金額が10桁を超えていないことを確認する
				if (commoditysales > 9999999999L ) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				// リストのクリア
				line.clear();
				try {
					br.close();
				} catch (IOException e) {

				}
			} catch (IOException e) {

			} finally {

			}
		}
		// フォルダを開く
		try {

			File file = new File(dir, "branch.out");
			// ファイルの有無を確認する
			if ( !(file.exists()) ) {
				// 無ければ新しく作成する
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bw);
			try {
				for (int i = 0; i < branch.size(); i++) {
				pw.println(branchnumber.get(i) + "," + branchsale.get(branchnumber.get(i))+  "," + branch.get(branchnumber.get(i)));
				//branchtest.put((branchnumber.get(i) + "," + branch.get(branchnumber.get(i))),branchsale.get(branchnumber.get(i)));

				}
			}finally {
				pw.close();	// メモリの開放
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
		}
		// フォルダを開く
		try {

			File file = new File(dir, "commodity.out");
			// ファイルの有無を確認する
			if ( !(file.exists()) ) {
				// 無ければ新しく作成する
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bw);
			try {
				for (int i = 0; i < commodity.size(); i++) {
					pw.println(commoditynumber.get(i) + "," +commodity.get(commoditynumber.get(i)) + "," + commoditysale.get(commoditynumber.get(i)));
				}
			}finally {
				pw.close();	// メモリの開放
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
		}
	}

}